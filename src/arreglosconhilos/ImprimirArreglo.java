package arreglosconhilos;

public class ImprimirArreglo extends Thread{
	Arreglo A,B,C;
	int max;
	
	public ImprimirArreglo(Arreglo A, Arreglo B, Arreglo C, int max) {
		this.A = A;
		this.B = B;
		this.C = C;
		this.max = max;
	}
	
	public void run() {
		try {
			sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Arreglo: A");
		A.imprimirDatos(max);
		System.out.println("Arreglo: B");
		B.imprimirDatos(max);
		System.out.println("Arreglo: C");
		C.imprimirDatos(max);
	}
}
