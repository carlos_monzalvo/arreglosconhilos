package arreglosconhilos;

public class Sumador extends Thread{
	
	Arreglo A,B,C;
	int dato,casilla,datoA, datoB;
	
	public Sumador(Arreglo A, Arreglo B, Arreglo C) {
		this.A = A;
		this.B = B;
		this.C = C;
	}
	
	public void run() {
		while(!C.terminar(C.arreglo.length-1)) {
			
			casilla = C.getIndice();
			datoA = A.getDato(casilla, getName());
			datoB = B.getDato(casilla, getName());
			dato  = datoA + datoB ;
			C.almacenar(casilla,dato, getName());
			
			System.out.println("\t El hilo SUMADOR: " + getName() + " Guardo: "+ datoA +" + "+ datoB +" = " + dato + " EN: " + casilla);
		}
		System.out.println("\t Termino El hilo SUMADOR:" +getName());
	}
}
