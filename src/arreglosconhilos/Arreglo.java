package arreglosconhilos;

public class Arreglo {
	int []arreglo;
	int indice = -1;
	char nombre;
	
	public Arreglo(int[] arreglo, char nombre) {
		this.arreglo = arreglo;
		this.nombre = nombre;
	}
	
	public  boolean terminar(int max) {
		if(indice == max) {
			return true;
		}
		return false;
	}
	public void imprimirDatos(int total) {
		for(int i= 0; i<= total-1; i++) {
			System.out.println("\t"+getDato(i, "hiloimpresion"));
		}
	}
	
	public synchronized boolean almacenar(int dato, String hilo, char nombreArr) {
		if(indice >= arreglo.length-1) {
			return false;
		}
		if(indice <= arreglo.length-1) {
			notifyAll();
			indice++;
			System.out.println("El hilo PRODUCTOR agrego el dato "+nombreArr+"["+indice+"]"+": "+ dato +" El hilo: " + hilo);
			arreglo[indice] = dato;
			return true;
		}
		return false;
	}
	
	public synchronized void almacenar(int casilla, int dato, String hilo) {
		if(indice > arreglo.length-1) {
			return;
		}
		if(indice <= arreglo.length-1) {
			arreglo[casilla] = dato;
			notifyAll();
		}
	}
	
	public synchronized int getIndice() {
		
		if(indice == arreglo.length-1) {
			return arreglo.length-1;
		}
		indice++;
		return indice;
	}
	
	public synchronized int getDato(int casilla, String hilo) {
		while(indice < casilla) {
			try {
				System.out.println("El hilo SUMADOR esta esperando: "+ hilo);
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("El hilo SUMADOR: "+ hilo +" se Activo---->");
		}
		notifyAll();
		return arreglo[casilla];
	}
}