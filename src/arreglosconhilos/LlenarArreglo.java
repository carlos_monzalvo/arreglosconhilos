package arreglosconhilos;

public class LlenarArreglo extends Thread {
	Arreglo arreglo;
	int dato;
	char nombreArr;
	
	public LlenarArreglo(Arreglo arreglo, char nombreArr) {
		this.arreglo = arreglo;
		this.nombreArr = nombreArr;
	}
	
	public void run() {
		dato = (int)(Math.random()*100);
		while(arreglo.almacenar(dato, getName(), nombreArr)) {
			dato = (int)(Math.random()*100);
		}
		System.out.println("\t Arreglo "+ nombreArr +" Termino el hilo... " + getName());
	}
}