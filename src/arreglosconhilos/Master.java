package arreglosconhilos;

public class Master {

	public static void main(String[] args) throws InterruptedException {
		int [] arrA,arrB,arrC;
		Arreglo A,B,C;
		LlenarArreglo []hilosLlenadoresA;
		LlenarArreglo []hilosLlenadoresB;
		ImprimirArreglo imprimir;
		Sumador []sumador;
		int max, numHilos;

		max = Teclado.entero("Dame tamaño de los Arreglos: ");
		numHilos = Teclado.entero("Dame el numero de hilos: ");
		
		hilosLlenadoresA = new LlenarArreglo[numHilos];
		hilosLlenadoresB = new LlenarArreglo[numHilos];
		sumador= new Sumador[numHilos];
		arrA = new int[max];
		arrB = new int[max];
		arrC = new int[max];
		
		A = new Arreglo(arrA, 'A');
		B = new Arreglo(arrB, 'B');
		C = new Arreglo(arrC, 'C');
		
		for(int i = 0; i<= numHilos-1; i++) {
			hilosLlenadoresA[i] = new LlenarArreglo(A,'A');
			hilosLlenadoresB[i] = new LlenarArreglo(B,'B');
			sumador[i] = new Sumador(A, B, C);
			hilosLlenadoresB[i].start();
			hilosLlenadoresA[i].start();
			sumador[i].start();
		}
		
		imprimir = new ImprimirArreglo(A, B, C, max);
		imprimir.start();		
	}
}
